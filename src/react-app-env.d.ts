/// <reference types="react-scripts" />

declare module 'react-router-dom'
declare module 'styled-components'
declare module '@atlaskit/field-text'
declare module '@atlaskit/select'
declare module '@atlaskit/item'
declare module '@atlaskit/droplist'