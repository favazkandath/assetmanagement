import React from 'react';
import './App.css';
import {BrowserRouter as Router} from 'react-router-dom'
import Routes from './Routes'
import firebase from "firebase/app";

class App extends React.Component<any,any> {
    private firebaseConfig: { storageBucket: string; apiKey: string; messagingSenderId: string; appId: string; projectId: string; databaseURL: string; authDomain: string };

    constructor(props: any) {
        super(props)
        this.state = {
            initialized: false
        }
        this.firebaseConfig = {
            apiKey: "AIzaSyAvbM-K7YSgPhz9qyXyL0GuTVV8jrCSCeU",
            authDomain: "projectx-a3574.firebaseapp.com",
            databaseURL: "https://projectx-a3574.firebaseio.com",
            projectId: "projectx-a3574",
            storageBucket: "projectx-a3574.appspot.com",
            messagingSenderId: "842045696592",
            appId: "1:842045696592:web:3d1b872e8213af43867f55"
          }
    }

    componentDidMount() {
        if(!firebase.apps.length) {
            firebase.initializeApp(this.firebaseConfig)
            this.setState({ initialized: true })
        }
    }

    render() {
        console.log(this.state.initialized)
        return (
            <div className="App">
                {this.state.initialized && <Router><Routes/></Router>}
            </div>
        )
    }

}

export default App;
