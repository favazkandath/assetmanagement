import * as React from 'react';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import TextField from '@material-ui/core/TextField'
import { readAssets, editAssetById, readUsers } from '../../src/main/db/api';
import Item from '@atlaskit/item';
import DropList from '@atlaskit/droplist';
import ChevronDownIcon from '@atlaskit/icon/glyph/chevron-down';
import icon from '../assets/mac.jpg'
import icon2 from '../assets/fawaz.jpg'
import styled from 'styled-components'

const Text = styled.div`
    font-size: 24px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.21;
    letter-spacing: normal;
    text-align: left;
    color: #484848;
`

class PreviewEmployee extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            assetsArray: [],
            selectedName: '',
            isOpen: false
        }
    }

    componentDidMount() {
        if (this.props.type === 'user') {
            readAssets()
                .then(async response => {
                    const responseArray: any = []
                    await response.forEach((item: any) => {
                        responseArray.push({
                            id: item.key,
                            value: item.val()
                        })
                    })
                    this.setState({ assetsArray: responseArray })
                })
        } else {
            readUsers()
            .then(async response => {
                const responseArray: any = []
                await response.forEach((item: any) => {
                    responseArray.push({
                        id: item.key,
                        value: item.val()
                    })
                })
                this.setState({ assetsArray: responseArray, selectedName:  this.props.item.value.assignee ? this.props.item.value.assignee : ''})
            })
        }
    }

    handleAssign = () => {
        if ( this.props.type === 'user' ) {
            editAssetById(this.state.selectedName.value.name, this.state.selectedName.value.serialno, this.state.selectedName.value.type, this.props.id)
            this.props.close()
        } else {
            editAssetById(this.props.item.value.name, this.props.item.value.serialno, this.props.item.value.type, this.state.selectedName.id ? this.state.selectedName.id : '')
            this.props.close()
        }
    }

    render() {
        return (
            <div>
                < ModalTransition >
                    <Modal
                        actions={[
                            {
                                text: 'Cancel', onClick: () => this.props.close()
                            }, {
                                text: 'Assign', onClick: () => this.handleAssign()
                            }
                        ]}
                        onClose={() => this.props.close()} heading={this.props.type === 'user' ? `What asset do you want to assign to ${this.props.title} ?` : `Whom do you want to assign ${this.props.title} to ?`}
                    >
                    
                    <TextField
                        id="outlined-name"
                        value={this.props.type === 'user' ? (this.state.selectedName && this.state.selectedName.value.name) : (this.state.selectedName.id ? this.state.selectedName.id : this.state.selectedName)}
                        label={this.props.type === 'user' ? 'Asset' : 'User'}
                        margin="normal"
                        variant="outlined"
                        disabled={true}
                    />

                    <DropList
                        appearance="default"
                        position="left top"
                        isTriggerNotTabbable
                        onClick={() => this.setState({ isOpen: !this.state.isOpen})}
                        isOpen={this.state.isOpen}
                        trigger={<div style={{ cursor: 'pointer', position: 'relative', top:'2rem', right: '2rem' }}><ChevronDownIcon label={'down'}/></div>}
                    >
                        <Item onClick={() => this.setState({ selectedName: '' })}>Unassigned</Item>
                        {
                            this.state.assetsArray.map((item: any) => (
                                <Item onClick={() => this.setState({ selectedName: item })}>
                                    <div style={{display: 'grid', gridTemplateColumns: '4rem 8rem', justifyItems: 'center'}}>
                                        <div><img src={this.props.type === 'user' ? icon : icon2} style={{height: '3rem', width: '3rem', borderRadius: '1.5rem'}}/></div>
                                        <div>
                                            <div style={{width: 'fit-content', padding: '0rem 1rem', background: '#8e8e8e', color: '#ffffff', borderRadius: '4px'}}>{this.props.type === 'user' ? item.value.serialno : item.value.employee_id}</div>
                                            <Text>{this.props.type === 'user' ? item.value.name : item.value.user_name}</Text>
                                        </div>
                                    </div>
                                </Item>
                            ))
                        }
                    </DropList>
                    </Modal>
                </ModalTransition >
            </div >
        )
    }
}

export default PreviewEmployee