import * as React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { createAssets, readAssetById, editAssetById } from '../main/db/api';
import styled from 'styled-components'
import { Heading } from "../main/dashboard/Dashboard";
import Avatar from "react-avatar-edit";


const Title = styled.div`
font-family: 'IBM Plex Sans', sans-serif;
font-size: 45px;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: 1.22;
letter-spacing: normal;
text-align: left;
color: #616161;
`

const Required = styled.div`
  font-size: 24px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.21;
  letter-spacing: normal;
  text-align: left;
  color: #484848;
`

const Image = styled.img`
    width: 70px;
    height: 70px;
    border-radius: 35px;
    background: #838383;
    cursor: pointer;
`
const Description = styled.div`
    padding-bottom: 1rem;
    font-family: 'IBM Plex Sans', sans-serif;
    font-size: 18px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.21;
    letter-spacing: normal;
    text-align: left;
    color: #484848;
`

// const options = [
//   {
//     label: 'Laptop',
//     value: 'Laptop'
//   },
//   {
//     label: 'Keyboard',
//     value: 'Keyboard'
//   },
//   {
//     label: 'Mouse',
//     value: 'Mouse'
//   },
//   {
//     label: 'Monitor',
//     value: 'Monitor'
//   },
//   {
//     label: 'HDMI',
//     value: 'hdmi'
//   },
//   {
//     label: 'Device',
//     value: 'Device'
//   }
// ]

interface IAddAsset {
  name: string
  serial: string
  type: string
  assignee: string
  displayClose: string
  src: any
  preview: any
  description: string
  tags: string
}

class AddAsset extends React.Component<any, IAddAsset> {
  constructor(props: any) {
    super(props);
    this.state = {
      name: "",
      serial: '',
      type: "",
      assignee: '',
      displayClose: 'none',
      src: undefined,
      preview: undefined,
      description: '',
      tags: ''
    };
  }

  inputLabel: any
  componentDidMount() {
    if (this.props && this.props.match && this.props.match.params && this.props.match.params.id) {
        readAssetById(this.props.match.params.id)
        .then(response => {
          console.log(response.val())
            this.setState({
              name: response.val().name,
              serial: response.val().serialno ? response.val().serialno : [],
              type: response.val().type
            })
        })
    }
  }

  handleName = (event: any) => {
    this.setState({
      name: event.target.value
    });
  };

  handleSerial = (event: any) => {
    this.setState({
      serial: event.target.value,
    })
  }

  handleType = (event: any) => {
    this.setState({
      type: event.target.value
    });
  };

  handleSave = () => {
    if (this.props && this.props.match && this.props.match.params && this.props.match.params.id) {
      editAssetById(this.state.name, this.state.serial, this.state.type, this.state.assignee)
      this.props.history.push('/assets')
    } else {
      createAssets(
        this.state.name,
        this.state.serial,
        this.state.type,
        this.state.assignee,
        this.state.tags,
        this.state.description,
        this.state.src ? this.state.src : (this.state.preview ? this.state.preview : '')
      )
      this.props.history.push('/assets')
    }
  }
  

  render() {
    return (<div className="content-container">
    <div className="header" style={{display: 'grid', paddingLeft: '1rem'}}>
        <Heading>{this.state.name ? this.state.name : 'Add Asset'}</Heading>
    </div>
    <div className="content">
      <div className="addAsset" style={{padding: '1rem'}}>
        <Title>Enter asset details</Title>
        <Required style={{paddingTop:'2rem'}}>Required Information</Required>
        <div style={{display: 'grid', gridTemplateColumns: 'repeat(2,1fr)'}}>
          <div>
            <div className="input" style={{width: '20rem', paddingTop: '1rem'}}>
              <TextField
                  id="outlined-name"
                  value={this.state.name}
                  label='Name'
                  onChange={this.handleName}
                  margin="normal"
                  variant="outlined"
              />
            </div>
            <div className="input" style={{width: '20rem', paddingTop: '1rem'}}>
              <TextField
                    id="outlined-name"
                    value={this.state.type}
                    label={'Type'}
                    onChange={this.handleType}
                    margin="normal"
                    variant="outlined"
                />
            </div>
            <div style={{width: '20rem', paddingTop: '1rem'}}>
              <TextField
                    id="outlined-name"
                    label={'Serial no.'}
                    value={this.state.serial}
                    onChange={this.handleSerial}
                    margin="normal"
                    variant="outlined"
                />
            </div>
            <div style={{width: '20rem', paddingTop: '1rem'}}>
              <TextField
                    id="outlined-name"
                    label={'Tags'}
                    value={this.state.tags}
                    onChange={(event: any) => this.setState({ tags: event.target.value})}
                    margin="normal"
                    variant="outlined"
                />
            </div>
          </div>
          <div>
            <div>
                <Description>Add pictures of the asset</Description>
                <div>
                    <div style={{display: 'grid', gridTemplateColumns: '100px 100px', position: 'relative'}}>
                        {this.state.src && 
                        <>
                            <div 
                                onMouseOver={() => this.setState({ displayClose: 'unset' })}
                                onMouseLeave={() => this.setState({ displayClose: 'none' })}
                                onClick={() => this.setState({ src: undefined })}
                                style={{
                                    position: 'absolute', 
                                    height: '70px', 
                                    width: '70px', 
                                    borderRadius: '35px', 
                                    display: this.state.displayClose, 
                                    background: 'black',
                                    zIndex: 2,
                                    opacity: 0.5,
                                    cursor: 'pointer'
                                }}
                            >
                                <span style={{
                                    position: 'absolute',
                                    top: '1.5rem',
                                    color: 'white',
                                    left: '0.5rem',
                                    fontWeight: 'bolder'
                                }}>Change</span>
                            </div>
                            <Image 
                                src={this.state.src} 
                                alt=""
                                onMouseOver={() => this.setState({ displayClose: this.state.src ? 'unset' : 'none' })}
                            />
                        </>}
                        <Avatar
                            height={70}
                            width={70}
                            label={'+'}
                            borderStyle={{
                                borderRadius: 'none'
                            }}
                            labelStyle={{
                                fontFamily: "'IBM Plex Sans', sans-serif",
                                fontSize: '34px',
                                fontWeight: 'bold',
                                fontStyle: 'normal',
                                fontStretch: 'normal',
                                lineHeight: 1.24,
                                letterSpacing: 'normal',
                                color: '#ffffff',
                                display: 'grid',
                                justifyContent: 'center',
                                background: '#838383',
                                borderRadius: '35px',
                                border: 'none',
                                cursor: 'pointer'
                            }}
                            onCrop={(preview: any) => this.setState({ preview })}
                            onClose={() => this.setState({ src: this.state.preview })}
                            img={this.state.preview}
                        />
                    </div>
                    <Description style={{paddingTop: '2rem'}}>Description</Description>
                    <div style={{ width: '20rem' }}>
                        <TextField
                            autoComplete={'off'}
                            id="outlined-name"
                            value={this.state.description}
                            label={'Description'}
                            onChange={(event: any) => this.setState({ description: event.target.value })}
                            margin="normal"
                            variant="outlined"
                        />
                    </div>
                </div>
            </div>
          </div>
        </div>
        <Required style={{paddingTop: '1rem'}}>Optional Information</Required>
        <div style={{width: '20rem', paddingTop: '1rem'}}>
          <TextField
                id="outlined-name"
                label={'Assignee'}
                value={this.state.assignee}
                onChange={(event: any) => this.setState({ assignee: event.target.value})}
                margin="normal"
                variant="outlined"
            />
        </div>
        <div style={{ marginTop: '3rem', display: 'grid', justifyItems: 'end', marginBottom: '3rem', paddingBottom: '3rem'}}>
            <div style={{color: 'green'}}>
                <Button variant="outlined" style={{marginRight: '1rem'}} color="inherit"  onClick={() => this.props.history.push('/assets')}>Cancel</Button>
                <Button variant="contained" style={{ background: 'green' }} color="primary" onClick={() => this.handleSave()}>Save</Button>
            </div>
        </div>
      </div></div></div>
    );
  }
}
export default AddAsset;
