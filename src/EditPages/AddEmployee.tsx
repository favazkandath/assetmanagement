import * as React from 'react'
import TextField from "@material-ui/core/TextField"
import Button from '@material-ui/core/Button';
import styled from 'styled-components'
import { createUsers, readUserById, editUserById } from '../main/db/api';
import { Heading } from '../main/dashboard/Dashboard';
import Avatar from 'react-avatar-edit';

const EnterDetails = styled.div`
    font-family: 'IBM Plex Sans', sans-serif;
    font-size: 45px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.22;
    letter-spacing: normal;
    text-align: left;
    color: #616161;
`

const Password = styled.div`
    padding-top: 1rem;
    font-family: 'IBM Plex Sans', sans-serif;
    font-size: 18px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.21;
    letter-spacing: normal;
    text-align: left;
    color: #484848;
`

const Image = styled.img`
    width: 70px;
    height: 70px;
    border-radius: 35px;
    background: #838383;
    cursor: pointer;
`

interface IAddEmployee {
    ename: string
    eid: string
    email: string
    phone: string
    src: any
    displayClose: string
    preview: any
    userName: string
    password: any
    confirmPassword?: string
    jobTitle: string
    responsePass?: string
}

class AddEmployee extends React.Component<any, IAddEmployee> {
    constructor(props: any) {
        super(props)
        this.state = {
            ename: '',
            eid: '',
            email: '',
            phone: '',
            src: undefined,
            preview: undefined,
            displayClose: 'none',
            userName: '',
            password: undefined,
            confirmPassword: undefined,
            jobTitle: '',
            responsePass: undefined
        }
    }

    componentDidMount() {
        if (this.props && this.props.match && this.props.match.params && this.props.match.params.id && (this.props.history.location.pathname.includes('/new') === false)) {
            readUserById(this.props.match.params.id)
                .then(response => {
                    this.setState({
                        ename: response.val().empName,
                        eid: response.val().empID,
                        email: response.val().email,
                        phone: response.val().phoneNumber,
                        src: response.val().profile !== '' ? response.val().profile : undefined,
                        preview: undefined,
                        displayClose: 'none',
                        userName: response.val().userName,
                        password: undefined,
                        confirmPassword: undefined,
                        jobTitle: response.val().jobTitle,
                        responsePass: response.val().password
                    })
                })
        }
    }

    handleSave = () => {
        if (this.props && this.props.match && this.props.match.params && this.props.match.params.id) {
            editUserById(
                this.state.ename,
                this.state.userName,
                this.state.eid,
                this.state.email,
                this.state.phone,
                (this.state.password === '' && this.state.confirmPassword === '') ? this.state.responsePass : this.state.password,
                this.state.src ? this.state.src : (this.state.preview ? this.state.preview : ''),
                this.state.jobTitle
            )
            this.props.history.push('/employee')
        } else {
            createUsers(
                this.state.ename,
                this.state.userName,
                this.state.eid,
                this.state.email,
                this.state.phone,
                this.state.password,
                this.state.src ? this.state.src : (this.state.preview ? this.state.preview : ''),
                this.state.jobTitle
            )
            this.props.history.push('/employee')
        }
    }

    render() {
        return (<div className="content-container">
            <div className="header" style={{ display: 'grid', paddingLeft: '1rem' }}>
                <Heading>{this.props.history.location.pathname.includes('/new') ? 'New User' : this.state.ename}</Heading>
            </div>
            <div className="content">
                <div style={{
                    padding: '1rem',
                    borderRadius: '5px',
                    background: '#ffffff',
                    margin: '1rem',
                    marginRight: '5rem',
                    paddingBottom: '3rem'
                }}>
                    <EnterDetails>Enter your details</EnterDetails>
                    <div>
                        <h3>New Employee</h3>
                        <div style={{ display: 'grid', gridTemplateColumns: 'repeat(2,1fr)' }}>
                            <div>
                                <div style={{ width: '20rem' }}>
                                    <TextField
                                        id="outlined-name"
                                        value={this.state.eid}
                                        label={'Employee Id'}
                                        onChange={(event: any) => this.setState({ eid: event.target.value })}
                                        margin="normal"
                                        variant="outlined"
                                    />
                                </div>
                                <div style={{ width: '20rem' }}>
                                    <TextField
                                        id="outlined-name"
                                        value={this.state.ename}
                                        label={'Employee Name'}
                                        onChange={(event: any) => this.setState({ ename: event.target.value })}
                                        margin="normal"
                                        variant="outlined"
                                    />
                                </div>
                                <div style={{ width: '20rem' }}>
                                    <TextField
                                        id="outlined-name"
                                        value={this.state.userName}
                                        label={'User Name'}
                                        onChange={(event: any) => this.setState({ userName: event.target.value })}
                                        margin="normal"
                                        variant="outlined"
                                    />
                                </div>
                                <div style={{ width: '20rem' }}>
                                    <TextField
                                        id="outlined-name"
                                        value={this.state.email}
                                        label={'Email ID'}
                                        onChange={(event: any) => this.setState({ email: event.target.value })}
                                        margin="normal"
                                        variant="outlined"
                                    />
                                </div>
                                <div style={{ width: '20rem' }}>
                                    <TextField
                                        id="outlined-name"
                                        value={this.state.phone}
                                        label={'Phone Number'}
                                        onChange={(event: any) => this.setState({ phone: event.target.value })}
                                        margin="normal"
                                        variant="outlined"
                                    />
                                </div>
                            </div>
                            <div>
                                <h3>Add a profile picture</h3>
                                <div>
                                    <div style={{display: 'grid', gridTemplateColumns: '100px 100px', position: 'relative'}}>
                                        {this.state.src && 
                                        <>
                                            <div 
                                                onMouseOver={() => this.setState({ displayClose: 'unset' })}
                                                onMouseLeave={() => this.setState({ displayClose: 'none' })}
                                                onClick={() => this.setState({ src: undefined })}
                                                style={{
                                                    position: 'absolute', 
                                                    height: '70px', 
                                                    width: '70px', 
                                                    borderRadius: '35px', 
                                                    display: this.state.displayClose, 
                                                    background: 'black',
                                                    zIndex: 2,
                                                    opacity: 0.5,
                                                    cursor: 'pointer'
                                                }}
                                            >
                                                <span style={{
                                                    position: 'absolute',
                                                    top: '1.5rem',
                                                    color: 'white',
                                                    left: '0.5rem',
                                                    fontWeight: 'bolder'
                                                }}>Change</span>
                                            </div>
                                            <Image 
                                                src={this.state.src} 
                                                alt=""
                                                onMouseOver={() => this.setState({ displayClose: this.state.src ? 'unset' : 'none' })}
                                            />
                                        </>}
                                        <Avatar
                                            height={70}
                                            width={70}
                                            label={'+'}
                                            borderStyle={{
                                                borderRadius: 'none'
                                            }}
                                            labelStyle={{
                                                fontFamily: "'IBM Plex Sans', sans-serif",
                                                fontSize: '34px',
                                                fontWeight: 'bold',
                                                fontStyle: 'normal',
                                                fontStretch: 'normal',
                                                lineHeight: 1.24,
                                                letterSpacing: 'normal',
                                                color: '#ffffff',
                                                display: 'grid',
                                                justifyContent: 'center',
                                                background: '#838383',
                                                borderRadius: '35px',
                                                border: 'none',
                                                cursor: 'pointer'
                                            }}
                                            onCrop={(preview: any) => this.setState({ preview })}
                                            onClose={() => this.setState({ src: this.state.preview })}
                                            img={this.state.preview}
                                        />
                                    </div>
                                    <Password>Enter a strong password</Password>
                                    <div style={{ width: '20rem' }}>
                                        <TextField
                                            id="outlined-name"
                                            type={'password'}
                                            value={this.state.password}
                                            label={'Password'}
                                            onChange={(event: any) => this.setState({ password: event.target.value })}
                                            margin="normal"
                                            variant="outlined"
                                            autoComplete={'off'}
                                        />
                                    </div>
                                    <div style={{ width: '20rem' }}>
                                        <TextField
                                            type={'password'}
                                            autoComplete={'off'}
                                            error={this.state.password && this.state.confirmPassword && (this.state.password !== this.state.confirmPassword) ? true : false}
                                            id="outlined-name"
                                            value={this.state.confirmPassword}
                                            label={'Confirm Password'}
                                            onChange={(event: any) => this.setState({ confirmPassword: event.target.value })}
                                            margin="normal"
                                            variant="outlined"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Password>Additional Information</Password>
                        <div style={{ width: '20rem' }}>
                            <TextField
                                id="outlined-name"
                                value={this.state.jobTitle}
                                label={'Job Title'}
                                onChange={(event: any) => this.setState({ jobTitle: event.target.value })}
                                margin="normal"
                                variant="outlined"
                            />
                        </div>
                    <div style={{ marginBottom: '3rem', display: 'grid', justifyItems: 'end' }}>
                        <div style={{ color: 'green' }}>
                            <Button variant="outlined" style={{ marginRight: '1rem' }} color="inherit" onClick={() => this.props.history.push('/employee')}>Cancel</Button>
                            <Button variant="contained" style={{ background: 'green' }} color="primary" onClick={this.handleSave}>Save</Button>
                        </div>
                    </div>
                    </div>
                </div></div></div>
        )
    }
}
export default AddEmployee