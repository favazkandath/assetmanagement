import * as React from 'react'
import DynamicTable from '@atlaskit/dynamic-table'
import Button, { ButtonGroup } from '@atlaskit/button'
import EditFilledIcon from '@atlaskit/icon/glyph/edit-filled'
import TrashIcon from '@atlaskit/icon/glyph/trash'
import { readAssets, deleteAssetById } from '../db/api'
import AddCircleIcon from '@atlaskit/icon/glyph/add-circle';
import { Heading } from '../dashboard/Dashboard';
import icon from '../../assets/mac.jpg'
import Item from '@atlaskit/item';
import DropList from '@atlaskit/droplist';
import MoreVerticalIcon from '@atlaskit/icon/glyph/more-vertical';
import PreviewEmployee from '../../Preview/PreviewEmployee';


class Assets extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            assetArray: [],
            spinner: true,
            isOpen: '',
            preview: '',
            previewItem: ''
        }
    }
    componentDidMount() {
        readAssets()
            .then(async response => {
                console.log(response.val())
                const responseArray: any[] = []
                await response.forEach(item => {
                    responseArray.push({
                        value: item.val(),
                        id: item.key
                    })
                })
                this.setState({ assetArray: responseArray, spinner: false })
            })
    }

    handleDelete = (item: any) => {
        deleteAssetById(item.id)
            .then(response => this.componentDidMount())
    }

    handlePreview = (title: any, id: any, item: any) => {
        this.setState({
            preview: 'employee',
            previewItem: {
                title,
                id,
                item
            }
        })
    }

    render() {
        return (<div className="content-container">
            {this.state.preview === 'employee' && <PreviewEmployee type='asset' title={this.state.previewItem.title} id={this.state.previewItem.id} item={this.state.previewItem.item} close={() => this.setState({ preview: '' }, this.componentDidMount)}/>}
            <div className="header" style={{ display: 'grid', paddingLeft: '1rem' }}>
                <Heading>Assets</Heading>
                <div
                    style={{
                        position: 'fixed',
                        top: '8.5rem',
                        right: '2rem',
                        cursor: 'pointer'
                    }}
                    onClick={() => this.props.history.push('/assets/new')}
                ><AddCircleIcon label='add' size={'xlarge'} primaryColor={'#072d4d'} /></div>
            </div>
            <div className="content">
            <div className="employee" style={{ display: 'grid', gridTemplateColumns: 'repeat(3,1fr)', padding: '1rem' }}>
                    {this.state.assetArray.map((item: any, index: number) => (<div style={{ display: 'grid', gridTemplateColumns: '5rem auto auto', padding: '1rem', margin: '1rem', border: 'solid 1px #ffffff', borderRadius: '4px', backgroundColor: '#ffffff',boxShadow: '0 3px 6px 0 rgba(0, 0, 0, 0.16)' }}>
                        <div><img src={icon} style={{ borderRadius: '2.5rem', height: '5rem', width: '5rem' }} /></div>
                        <div style={{ paddingLeft: '1rem' }}>
                            <div style={{
                                width: 'fit-content',
                                background: '#8e8e8e',
                                borderRadius: '4px',
                                padding: '0rem 1rem',
                                color: '#ffffff'
                            }}>{item.value.assignee ? item.value.assignee : 'Unassigned'}</div>
                            <div style={{fontWeight: 'bold'}}>{item.value.name}</div>
                            <div>{item.value.serialno}</div>
                        </div>
                        <div style={{ justifySelf: 'end' }}>
                            <DropList
                                appearance="default"
                                position="left top"
                                isTriggerNotTabbable
                                onClick={() => this.setState({ isOpen: this.state.isOpen === ''? index : ''})}
                                isOpen={this.state.isOpen === index}
                                trigger={<div style={{ cursor: 'pointer' }}><MoreVerticalIcon label={'more'}/></div>}
                            >
                                <Item onClick={() => this.handleDelete(item)}><TrashIcon label='delete' size={"small"}/>Delete</Item>
                                <Item  onClick={() => this.props.history.push(`/assets/${item.id}`)}><EditFilledIcon label='edit' size={"small"}/>Edit</Item>
                                <Item disabled={item.value.assignee ? true : false} onClick={() => this.handlePreview(item.value.name, item.id, item)}><EditFilledIcon label='edit' size={"small"}/>Assign assets</Item>
                            </DropList>
                        </div>
                    </div>))}
                    </div>
            </div>
        </div>
        )
    }
}

export default Assets