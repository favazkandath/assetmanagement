import * as React from 'react'
import DynamicTable from '@atlaskit/dynamic-table'
import Button, { ButtonGroup } from '@atlaskit/button'
import TrashIcon from '@atlaskit/icon/glyph/trash'
import styled from 'styled-components'
import WatchFilledIcon from '@atlaskit/icon/glyph/watch-filled';
import { readUsers } from '../db/api';
import PreviewEmployee from '../../Preview/PreviewEmployee';

const SaveButton = styled.div`
    button {
        font-weight: 500;
    }
`

class Assign extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            employeeArray: [],
            spinner: true
        }
    }

    componentDidMount() {
        readUsers()
        .then(async response =>{
            const responseArray: any[] = []
            await response.forEach(item => {
                responseArray.push({
                    value: item.val(),
                    id: item.key
                })
            })
            await responseArray.map
            this.setState({ employeeArray: responseArray, spinner: false})
        })
    }

    handleDelete = (item: any) => {
        console.log(item)
        // deleteAssignAssetById(item.id)
        // this.componentDidMount()
    }

    handlePreview = (id: any, item: any) => {
        this.setState({ 
            preview: 'employee',
            previewItem: {
                'id': id,
                'item': item
            }
        })
    }

    render() {
        return (
            <div className="employee" style={{ padding: '1rem' }}>
            {this.state.preview === 'employee' && 
                <PreviewEmployee 
                    close={() => this.setState({preview: ''})}
                    previewItem={this.state.previewItem}
                />
            }
                <div style={{ display: 'grid', gridTemplateColumns: 'repeat(2,1fr)', alignItems: 'center' }}>
                    <h3>Assign Assets</h3>
                    <div style={{ justifySelf: 'end' }}>
                        <SaveButton><Button
                            appearance={'primary'}
                            onClick={() => this.props.history.push('/manage/assign')}
                        >
                            Manage Assets
                        </Button></SaveButton>
                    </div>
                </div>
                <div>View and manage assets here</div>
                <div style={{ border: 'solid 1px #dfe3e6', margin: '1rem 0rem' }} />
                    <DynamicTable
                        head={{
                            cells: [
                                {
                                    key: 'eId',
                                    content: 'Employee Id'
                                },
                                {
                                    key: 'eName',
                                    content: 'Employee Name'
                                },
                                {
                                    key: 'actions',
                                    content: 'Actions'
                                }
                            ]
                        }}
                        rows={this.state.employeeArray.map((item: any, index: number)=> 
                            ({
                                key: `${index}`,
                                cells: [
                                    {
                                        key: item.value.employee_id,
                                        content: item.value.employee_id
                                    },
                                    {
                                        key: item.value.user_name,
                                        content: item.value.user_name
                                    },
                                    {
                                        key: 'delete',
                                        content: (
                                            <ButtonGroup>
                                                <Button
                                                    onClick={() => this.handlePreview(item.id, item.value)}
                                                    iconBefore={<WatchFilledIcon label='edit' />}
                                                    appearance={'subtle'}
                                                />
                                                {/* <Button
                                                    onClick={() => this.handleDelete(item)}
                                                    iconBefore={<TrashIcon label='delete' />}
                                                    appearance={'subtle'}
                                                /> */}
                                            </ButtonGroup>
                                        )
                                    },
                                ]
                            }))
                        }
                        isLoading={this.state.spinner}
                        rowsPerPage={10}
                    />
            </div>
        )
    }
}

export default Assign