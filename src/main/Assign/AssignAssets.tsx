import * as React from "react";
import Select from "@atlaskit/select";
import { Checkbox } from "@atlaskit/checkbox";
import Button, { ButtonGroup } from "@atlaskit/button";
import { readUsers, readAssets } from "../db/api";

class AssignAssets extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      employeeArray: [],
      assetArray: [],
      selectedEmployee: undefined,
      selectedAsset: undefined
    };
  }
  componentDidMount() {
    readUsers()
      .then(async response => {
        const responseArray: any[] = []
        await response.forEach(item => {
          responseArray.push({
            value: item.val(),
            id: item.key
          })
        })
        this.setState({ 
          employeeArray: responseArray.map(item => ({
            label: <div style={{ color: 'black' }}>{item.value.user_name}</div>,
            value: item
          }))
        })
      })
  }

  loadAssets = () => {
    readAssets()
      .then(async response => {
        const responseArray: any[] = []
        await response.forEach(item => {
          responseArray.push({
            value: item.val(),
            id: item.key
          })
        })
        this.setState({ 
          assetArray: responseArray.map(item => ({
            label: <div style={{ color: 'black' }}>{item.value.name}-{item.value.serialno}</div>,
            value: item
          }))
        })
      })
  }

  handleEmployee = (item: any) => {
    this.setState({
      selectedEmployee: item.value
    }, this.loadAssets)
  }

  handleSave = () => {
    this.props.history.push('/manage')
  }

  render() {
    return (
      <div className="employee" style={{ padding: "1rem" }}>
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "repeat(2,1fr)",
            alignItems: "center"
          }}
        >
          <h3>Manage Assets</h3>
        </div>
        <div className="select" style={{ display: "flex", marginTop: "2rem" }}>
          <div style={{ width: "30%", marginRight: "10%" }}>
            <div style={{ marginBottom: '1rem' }}>Employee</div>
            <Select aria-label="Employee" name="employee" options={this.state.employeeArray} onChange={(event: any) => this.handleEmployee(event)} />
          </div>
          <div style={{ width: "30%", marginRight: "10%" }}>
            <div style={{ marginBottom: '1rem' }}>Assets</div>
            <Select
               aria-label="Employee"
              name="employee"
              options={this.state.assetArray}
              onChange={(event: any) => this.setState({ selectedAsset: event.value })} />
          </div>
        </div>
        {/* <div
          className="check"
          style={{
            display: "grid",
            gridTemplateColumns: "repeat(3,1fr)",
            alignItems: "center",
            marginTop: '1rem'
          }}
        >
        </div> */}
        <div className="button" style={{ marginTop: "2rem" }}>
          <ButtonGroup>
            <Button label="Cancel" appearance={"default"} onClick={() => this.props.history.push('/manage')}>Cancel</Button>
            <Button label="Save" appearance={"primary"} onClick={this.handleSave}> Save </Button>
          </ButtonGroup>
        </div>
      </div>
    );
  }
}

export default AssignAssets;
