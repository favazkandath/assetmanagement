import firebase from "firebase/app";
import 'firebase/database'

export const createUsers = (empName: string, userName: string, empID: string, email: string, phoneNumber: string, password: string, profile: any, jobTitle: string) => {
    firebase.database().ref(`users/${userName}-${empID}`).set({
        empID,
        empName,
        userName,
        password,
        phoneNumber,
        email,
        profile,
        jobTitle
    });
}
export const createAssets = (name: string, serialno: string, type: string, assignee: string, tags: string, description: any, profile: any) => {
    firebase.database().ref(`assets/${name}-${serialno}`).set({
        name,
        serialno,
        type,
        assignee,
        tags,
        description,
        profile
    });
}

export const readUsers = () => firebase.database().ref('/users/').once('value');

export const readUserById = (id: string) => firebase.database().ref(`/users/${id}`).once('value');

export const deleteUserById = (id: string) => firebase.database().ref(`/users/${id}`).remove()

export const editUserById = (empName: string, userName: string, empID: string, email: string, phoneNumber: string, password: string, profile: any, jobTitle: string) => {
    firebase.database().ref(`/users/${empID}`).update({
        empID,
        empName,
        userName,
        password,
        phoneNumber,
        email,
        profile,
        jobTitle
    })
}

export const readAssets = () => firebase.database().ref('/assets/').once('value');

export const readAssetById = (id: string) => firebase.database().ref(`/assets/${id}`).once('value');

export const deleteAssetById = (id: string) => firebase.database().ref(`/assets/${id}`).remove()

export const editAssetById = (name: string, serialno: string, type?: any, assignee?: string) => {
    firebase.database().ref(`/assets/${name}-${serialno}`).update({
        name: name,
        serialno: serialno,
        type: type,
        assignee: assignee,
    })
}
