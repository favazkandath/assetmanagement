import * as React from 'react';
import { Heading } from '../dashboard/Dashboard';
import icon from '../../assets/img.jpg'
import styled from 'styled-components'

const Text = styled.div`
    font-size: 36px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.22;
    letter-spacing: normal;
    text-align: left;
    color: #484848;
    justify-self: center;
`

class Home extends React.Component<any, any> {

    render() {
        return (<div className="content-container">
            <div className="header" style={{ display: 'grid', paddingLeft: '1rem' }}>
                <Heading>Get Started</Heading>
            </div>
            <div className="content">
                <div style={{display: 'grid', gridTemplateColumns: 'repeat(2, 1fr)', padding: '1rem'}}>
                    <div style={{ margin: '1rem', display: 'grid', gridTemplateRows: 'auto auto', borderRadius: '4px', background: '#ffffff', boxShadow: '0 3px 6px 0 rgba(0, 0, 0, 0.16)'}}>
                        <div><img src={icon} style={{height: '15rem', width: '33rem'}}/></div>
                        <Text>Add your first asset</Text>
                    </div>
                    <div style={{ margin: '1rem', display: 'grid', gridTemplateRows: 'auto auto', borderRadius: '4px', background: '#ffffff', boxShadow: '0 3px 6px 0 rgba(0, 0, 0, 0.16)'}}>
                        <div><img src={icon} style={{height: '15rem', width: '33rem'}}/></div>
                        <Text>Assign your existing asset</Text>
                    </div>
                    <div style={{ margin: '1rem', display: 'grid', gridTemplateRows: 'auto auto', borderRadius: '4px', background: '#ffffff', boxShadow: '0 3px 6px 0 rgba(0, 0, 0, 0.16)'}}>
                        <div><img src={icon} style={{height: '15rem', width: '33rem'}}/></div>
                        <Text>Invite more users</Text>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export default Home