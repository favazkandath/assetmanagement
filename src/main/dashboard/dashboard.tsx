import React from 'react';
import './Dashboard.scss'
import { Route, withRouter } from "react-router-dom"
// import Login from '../login/Login'
import Employee from '../employee/Employee'
import AddEmployee from '../../EditPages/AddEmployee'
import Assets from '../Assets/Assets'
import AddAsset from '../../EditPages/AddAsset'
import AssignAssets from '../Assign/AssignAssets'
import Assign from '../Assign/Assign';
import SidePanel from './SidePanel'
import styled from 'styled-components';
import Home from '../Home/Home';

export const Heading = styled.div`
    font-family: 'IBM Plex Sans', sans-serif;
    font-size: 55px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.22;
    letter-spacing: normal;
    text-align: left;
    color: #ffffff;
    align-self: end;
    padding-bottom: 1rem;
`;

class Dashboard extends React.Component<any, any>{
    render() {
        return (
            <div className="container">
                <SidePanel />
                <Route exact={true} path="/employee" render={(props: any) => <Employee {...props} />} />
                <Route exact={true} path="/assets" render={(props: any) => <Assets {...props} />} />
                <Route exact={true} path="/employee/new" render={(props: any) => <AddEmployee {...props} />} />
                {this.props.history.location.pathname.includes('/new') === false && <Route exact={true} path="/employee/:id" render={(props: any) => <AddEmployee {...props} />} />}
                <Route exact={true} path="/assets/new" render={(props: any) => <AddAsset {...props} />} />
                {this.props.history.location.pathname.includes('/new') === false && <Route exact={true} path="/assets/:id" render={(props: any) => <AddAsset {...props} />} />}
                <Route exact={true} path="/homepage" render={(props: any) => <Home {...props} />} />
            </div>
        );
    }
}

export default withRouter(Dashboard)