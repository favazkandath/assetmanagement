import * as React from 'react';
import HomeIcon from '../../assets/HomeIcon'
import icon from '../../assets/shoonya-icon.svg';
import styled from 'styled-components'
import Badge from '@material-ui/core/Badge';
import { Link, withRouter } from "react-router-dom"

const NavDrawer = styled(Link)`
    text-decoration: none;
    font-family: "IBM Plex Sans",sans-serif;
    font-size: 18px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.22;
    letter-spacing: normal;
    text-align: left;
    color: #ffffff;
`

const Wrapper = styled.div`
    padding: .5rem;
    margin: 1rem;
    border-radius: 30px;
    :hover {
        background: #274a67;
        cursor: pointer;
    }
`

class SidePanel extends React.Component <any, any> {

    render() {
        return (
            <div className="sidepanel">
            <div style={{padding: '1rem', display: 'grid', justifyItems: 'center', alignItems: 'center'}}>
                <img onClick={() => this.props.history.push('/homepage')} alt={'img'} src={icon} className="shoonyaicon" />
            </div>
            <Wrapper style={this.props.history.location.pathname.includes('/homepage') ? {backgroundColor: '#274a67'}: {}}>
                <NavDrawer to="/homepage">
                    <div style={{
                        display: 'grid',
                        gridTemplateColumns: '2rem auto'
                    }}><HomeIcon style={{fill: 'white'}}/>Home</div>
                </NavDrawer>
            </Wrapper>
            <Wrapper style={this.props.history.location.pathname.includes('/employee') ? {backgroundColor: '#274a67'}: {}}>
                <NavDrawer to="/employee">
                    <div style={{
                        display: 'grid',
                        gridTemplateColumns: '2rem auto'
                    }}><HomeIcon style={{fill: 'white'}}/>Employee</div>
                </NavDrawer>
            </Wrapper>
            <Wrapper style={this.props.history.location.pathname.includes('/assets') ? {backgroundColor: '#274a67'}: {}}>
                <NavDrawer to="/assets">
                    <div style={{
                        display: 'grid',
                        gridTemplateColumns: '2rem auto'
                    }}><HomeIcon style={{fill: 'white'}}/>Assets</div>
                </NavDrawer>
            </Wrapper>
            <Wrapper style={this.props.history.location.pathname.includes('/manage') ? {backgroundColor: '#274a67'}: {}}>
                <NavDrawer to="/manage">
                <Badge color="secondary" badgeContent={"NEW"}>
                <div style={{
                        display: 'grid',
                        paddingRight: '1rem',
                        gridTemplateColumns: '2rem auto'
                    }}><HomeIcon style={{fill: 'white'}}/>Reports</div>
        </Badge>
                   
                </NavDrawer>
            </Wrapper>
        </div>
        )
    }
}

export default withRouter(SidePanel)