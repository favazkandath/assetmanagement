import * as React from 'react'
import DynamicTable from '@atlaskit/dynamic-table'
import Button, { ButtonGroup } from '@atlaskit/button'
import EditFilledIcon from '@atlaskit/icon/glyph/edit-filled'
import TrashIcon from '@atlaskit/icon/glyph/trash'
import styled from 'styled-components'
import AddCircleIcon from '@atlaskit/icon/glyph/add-circle';
import { readUsers, deleteUserById, readUserById } from '../db/api';
import { Heading } from '../dashboard/Dashboard';
import icon from '../../assets/fawaz.jpg';
import Item from '@atlaskit/item';
import DropList from '@atlaskit/droplist';
import PreviewEmployee from '../../Preview/PreviewEmployee';
import MoreVerticalIcon from '@atlaskit/icon/glyph/more-vertical';

class Employee extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            employeeArray: [],
            spinner: true,
            preview: '',
            previewItem: '',
            isOpen: ''
        }
    }

    componentDidMount() {
        readUsers()
            .then(async response => {
                const responseArray: any[] = []
                await response.forEach(item => {
                    responseArray.push({
                        value: item.val(),
                        id: item.key
                    })
                })
                this.setState({ employeeArray: responseArray, spinner: false })
            })
    }

    handleDelete = (item: any) => {
        deleteUserById(item.id)
        this.componentDidMount()
    }

    handleEdit = (item: any) => {
        console.log(item.id)
        readUserById(item.id)
        this.props.history.push(`/employee/${item.id}`)
    }

    handlePreview = (title: any, id: any) => {
        this.setState({
            preview: 'employee',
            previewItem: {
                title,
                id
            }
        })
    }

    handleOpen = (index: number) => {
        this.setState({isOpen: this.state.isOpen === '' ? index : ''})
    }

    render() {
        return (<div className="content-container">
            {this.state.preview === 'employee' && <PreviewEmployee type='user' title={this.state.previewItem.title} id={this.state.previewItem.id} close={() => this.setState({ preview: '' }, this.componentDidMount)}/>}
            <div className="header" style={{ display: 'grid', paddingLeft: '1rem' }}>
                <Heading>Users</Heading>
                <div
                    style={{
                        position: 'fixed',
                        top: '8.5rem',
                        right: '2rem',
                        cursor: 'pointer'
                    }}
                    onClick={() => this.props.history.push('/employee/new')}
                ><AddCircleIcon label='add' size={'xlarge'} primaryColor={'#072d4d'} /></div>
            </div>
            <div className="content">
                <div className="employee" style={{ display: 'grid', gridTemplateColumns: 'repeat(3,1fr)', padding: '1rem' }}>
                    {this.state.employeeArray.map((item: any, index: number) => (<div style={{ display: 'grid', gridTemplateColumns: '5rem auto auto', padding: '1rem', margin: '1rem', border: 'solid 1px #ffffff', borderRadius: '4px', backgroundColor: '#ffffff',boxShadow: '0 3px 6px 0 rgba(0, 0, 0, 0.16)' }}>
                        <div><img src={item.value.profile !== '' ? item.value.profile : icon} style={{ borderRadius: '2.5rem', height: '5rem', width: '5rem' }} /></div>
                        <div style={{ paddingLeft: '1rem' }}>
                            <div style={{
                                width: 'fit-content',
                                background: '#8e8e8e',
                                borderRadius: '4px',
                                padding: '0rem 1rem',
                                color: '#ffffff'
                            }}>{item.value.empID}</div>
                            <div style={{fontWeight: 'bold'}}>{item.value.empName}</div>
                            <div>{item.value.email}</div>
                            <div>{item.value.phoneNumber}</div>
                        </div>
                        <div style={{ justifySelf: 'end' }}>
                            <DropList
                                appearance="default"
                                position="left top"
                                isTriggerNotTabbable
                                onClick={() => this.handleOpen(index)}
                                isOpen={this.state.isOpen === index}
                                trigger={<div style={{ cursor: 'pointer' }}><MoreVerticalIcon label={'more'}/></div>}
                            >
                                <Item onClick={() => this.handleDelete(item)}><TrashIcon label='delete' size={"small"}/>Delete</Item>
                                <Item onClick={() => this.handleEdit(item)}><EditFilledIcon label='edit' size={"small"}/>Edit</Item>
                                <Item onClick={() => this.handlePreview(item.value.user_name, item.id)}><EditFilledIcon label='edit' size={"small"}/>Assign assets</Item>
                            </DropList>
                        </div>
                    </div>))}
                </div>
            </div>
        </div>
        )
    }
}

export default Employee