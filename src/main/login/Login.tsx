import React from "react";
import styled from "styled-components";
import check from "../../../src/check.png";
import AboutUsFounderBackground from "../../../src/about_us_founder_background.svg";
import Andi from "../../../src/andi-signup.png";
import axios from "axios";

const BookDemoContainer = styled.div`
  position: relative;
  height: 100vh;
  background: linear-gradient(1deg, #f1fdff, #ffffff),
    linear-gradient(160deg, #ffff 80%, #fff 80%);
`;
const SectionParent = styled.section`
  padding: 0rem 18rem;
  @media (max-width: 1680px) and (min-width: 1440px) {
    padding: 0rem 14rem;
  }
  @media (max-width: 1440px) and (min-width: 768px) {
    padding: 0rem 5rem;
  }
  @media (max-width: 768px) {
    padding-top: 9px;
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media (max-width: 520px) {
    padding: 1rem;
  }
`;
const H1 = styled.h1`
  font-family: "BlenderPro", sans-serif;
  margin: 0;
  padding: 0;
  font-weight: normal;
  font-size: 28px;
  @media (max-width: 1440px) {
    font-size: 28px;
  }
  @media (max-width: 768px) {
    font-size: 24px;
  }
  @media (max-width: 520px) {
    text-align: center;
    font-size: 20px;
  }
`;
const Button = styled.button`
  height: 3rem;
  padding: 0rem 2rem;
  border-radius: 2px;
  cursor: pointer;
  margin: 10px;
  text-transform: uppercase;
  overflow: hidden;
  border-width: 0;
  outline: none;
  border-radius: 4px;
  box-shadow: 0 0px 0px rgba(0, 0, 0, 0);
  color: #fff;
  font-family: "BlenderPro", sans-serif;
  font-size: 16px;
  @media (max-width: 768px) {
    font-size: 14px;
  }
  @media (max-width: 520px) {
    font-size: 12px;
    padding: 0rem 1rem;
    height: 2.5rem;
  }
  /* font-weight: 600; */
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  text-rendering: geometricprecision;
  letter-spacing: normal;
  transition: background-color 0.3s;
`;

const BlueButton = styled(Button)`
  background-color: #01697b;
  :hover {
    background-color: #263163;
  }
  :active {
    background-color: #573b77;
  }
`;
const BookDemoPageContainer = styled(SectionParent)`
  padding-top: 10rem !important;
  display: grid;
  z-index: 1;
  position: relative;
  @media (max-width: 768px) {
    padding-top: 5rem !important;
  }
  background-repeat: no-repeat;
  background-size: cover;
`;
const RequestDemo = styled(H1)`
  color: #ffffff;
  letter-spacing: -0.85px;
  margin-top: 2rem;
  font-family: "BlenderPro", sans-serif;
  font-weight: 500;
  justify-self: center;
  margin-left: 2rem;
  @media (max-width: 768px) {
    text-align: center;
    width: 100%;
    margin-top: 3rem;
  }
`;
const BookDemoBg = styled.div`
  background-image: linear-gradient(to bottom, #23316c 20%, #101b4b);
  height: 60%;
  position: absolute;
  width: 100%;
  z-index: 0;
`;

const BookDemoBg2 = styled.div`
  height: 100%;
  position: absolute;
  width: 100%;
  z-index: 0;
  background: url(${AboutUsFounderBackground});
  background-size: cover;
`;

const FormComponent = styled.div`
  display: grid;
  position: relative;
  width: 70%;
  margin-left: 18%;
  padding: 0;
  box-shadow: 3px 2px 26px 0 rgba(112, 112, 112, 0.15);
  grid-template-columns: 50% 50%;
  @media (max-width: 768px) {
    grid-template-columns: repeat(1, 1fr);
  }
  padding-bottom: 0rem !important;
  margin-bottom: 0rem;
  padding-top: 0rem !important;
  top: 5rem;
  justify-items: center;
`;

const FormComponentLeft = styled.div`
  padding: 7rem 6rem;
  background: #111a41;
  border-radius: 8px 0px 0px 8px;
  width: 100%;
  @media (max-width: 768px) {
    border-radius: 8px 8px 0px 0px;
  }
  @media (max-width: 520px) {
    padding: 7rem 5rem;
  }
  @media (max-width: 400px) {
    padding: 6rem 2rem;
  }
`;

const FormComponentLeftTitle = styled(H1)`
  font-family: "BlenderPro", sans-serif;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  text-align: left !important;
  letter-spacing: normal;
  color: #ffffff;
  @media (max-width: 320px) {
    font-size: 18px;
  }
`;

const FormComponentLeftList = styled.ul`
  /* margin-left: 40px; */
  padding-left: 0.5em;
  list-style: none;
`;

const FormComponentLeftListItem = styled.li`
  font-family: "Nunito Sans", sans-serif;
  font-size: 18px;

  @media (max-width: 768px) {
    font-size: 14px;
  }
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;

  color: #ffffff;
  background: url(${check}) no-repeat;
  padding: 0.3em 0px 0.1em 30px;
  background-size: 24px;
  background-position-y: center;
  @media (max-width: 320px) {
    font-size: 12px;
  }
`;

const FormComponentLeftSignup = styled(H1)`
  font-family: "BlenderPro", sans-serif;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.17;
  text-align: left;
  letter-spacing: normal;
  color: #3accb0;
  padding-right: 1rem;
  @media (max-width: 320px) {
    font-size: 18px;
    text-align: left;
  }
`;
const PartnerWithUsForm = styled.form`
  width: 100%;
  margin: auto;
  display: grid;
  justify-content: center;
  @media (max-width: 520px) {
    padding-top: 1rem;
  }
`;
const BlueButtonNew = styled(BlueButton)`
  width: 40%;
  margin-top: 2rem;
  justify-self: center;
`;

// @ts-ignore
const StyledInputs = styled.input`
  :focus {
    color: #495057;
    background-color: #fff;
    border-color: #80bdff;
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
    color: black;
    background-image: none;
  }
  box-shadow: ${(props: { error: boolean }) =>
    props.error ? "0 0 0 0.2rem #fd7e7e5e" : "none"};
  font-family: "Nunito Sans", sans-serif;
  font-size: 14px;
  @media (max-width: 520px) {
    font-size: 12px;
  }
  line-height: 1.43;
  ::placeholder {
    color: rgba(107, 124, 147, 0.6);
    opacity: 1; /* Firefox */
  }
  padding: 1.07em 0.5em;
  margin-top: 10px;
  height: 25px;
  width: 100%;
  border: 1px rgba(107, 124, 147, 0.43) solid;
  border-color: ${(props: { error: boolean }) =>
    props.error ? "#eb6161" : "rgba(107, 124, 147, 0.43)"};
  padding: 0.475rem 0.75rem;
  border-radius: 5px;
  background-position-y: center;
  background-position-x: 0.5rem;
  background-repeat: no-repeat;
  text-indent: 20px;
`;
const FormComponentRight = styled.div`
  padding: 2rem 2rem 1.5rem 2rem;
  background: #ffffff;
  width: 100%;
  border-radius: 0px 8px 8px 0px;
  @media (max-width: 520px) {
    padding: 0rem 1rem 2rem 1rem;
  }
  @media (max-width: 768px) {
    border-radius: 0px 0px 8px 8px;
  }
`;
const SignUpHeading = styled(H1)`
  color: #01697b;
  margin-top: 3rem;
  text-align: center;
  letter-spacing: -0.85px;
  font-family: "BlenderPro", sans-serif;
  font-weight: 500;
  margin-bottom: 24px;
  @media (max-width: 768px) {
    width: 100%;
    margin-top: 3rem;
  }
`;
const ImgDesktop = styled.img`
  position: absolute;
  top: 4rem;
  left: calc(50% - 1.5rem);
  height: 7rem;
  animation-name: example;
  animation-duration: 1s;
  animation-iteration-count: infinite;
  animation-timing-function: ease-in-out;
  animation-direction: alternate;
  @keyframes example {
    from {
      top: 2rem;
    }
    to {
      top: 4rem;
    }
  }
`;
const PasswordMatchError = styled.div`
  font-size: 14px;
  color: #ff0100;
  margin-top: 0rem;
    margin-bottom: 1rem;
  font-family: 'Nunito Sans', sans-serif;
`;
class Login extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      userName: "",
      password: "",
      error: false,
    };
  }
  handleUserName = (event: any) => {
    this.setState({
      userName: event.target.value
    });
  };
  handlepassword = (event: any) => {
    this.setState({
      password: event.target.value
    });
  };
  handleLogin = (event: any) => {
    const username = this.state.userName;
    const password = this.state.password;
    event.preventDefault();
    axios({
      url: "https://develop-api.shoonyacloud.com/api/login/",
      method: "POST",
      withCredentials: false,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      data: JSON.stringify({
        username,
        password
      })
    }).then(result => {
        this.setState({
            error: false
        }, () => {
            this.props.handleLogin(result);
            this.props.history.push('/homepage')
        })
    })
    .catch(error =>{
      this.setState({
          error: true
      })
    });
  };
  render() {
    return (
      <BookDemoContainer>
        <BookDemoBg />
        <BookDemoBg2 />
        <BookDemoPageContainer>
          <ImgDesktop src={Andi} />
          <RequestDemo>
            At Esper, we help manage your assets. We simplify a complex world!
          </RequestDemo>
        </BookDemoPageContainer>
        <FormComponent style={this.props.style}>
          <FormComponentLeft>
            <FormComponentLeftTitle>
              Build and Manage with Esper
            </FormComponentLeftTitle>
            <FormComponentLeftList>
              <FormComponentLeftListItem>
              Manage the aassets for each{" "}
              </FormComponentLeftListItem>
              <FormComponentLeftListItem>
               Convenient way for HR
              </FormComponentLeftListItem>
              <FormComponentLeftListItem>
                Assign and delete assets{" "}
              </FormComponentLeftListItem>
              <FormComponentLeftListItem>
                Every management in one single touch{" "}
              </FormComponentLeftListItem>
            </FormComponentLeftList>
            <FormComponentLeftSignup>
            Easily manage and assign in one go
            </FormComponentLeftSignup>
          </FormComponentLeft>
          <FormComponentRight>
            <PartnerWithUsForm>
              <SignUpHeading>LOGIN TO YOUR ACCOUNT</SignUpHeading>
              {this.state.error === true && (
                  <PasswordMatchError>
                    Login Failed! Please enter valid credentials
                  </PasswordMatchError>
                )}
              {/* <SignUpSubHeading>
                Sign up for your free trial today to get started with Esper.
              </SignUpSubHeading> */}
              <div style={{ display: "flex" }}>
                <div style={{ display: "grid", width: "100%" }}>
                  <label
                    style={{
                      fontFamily: "Nunito Sans",
                      color: "#172b4d",
                      fontSize: "14px",
                      textAlign: "left"
                    }}
                  >
                    Username<span style={{ color: "red" }}>*</span>
                  </label>
                  <StyledInputs
                    aria-label="Username"
                    autocomplete="off"
                    error={this.state.error}
                    // style={{
                    //   width: '99%',
                    //   backgroundImage: `url(${FirstNameIcon})`,
                    // }}
                    value={this.state.userName}
                    maxLength={50}
                    onChange={this.handleUserName}
                    placeholder={"Username"}
                    type="text"
                    name="username"
                  />
                </div>
              </div>
              <div style={{ display: "grid", marginTop: "24px" }}>
                <label
                  style={{
                    fontFamily: "Nunito Sans",
                    color: "#172b4d",
                    fontSize: "14px",
                    textAlign: "left"
                  }}
                >
                  Password <span style={{ color: "red" }}>*</span>
                </label>
                <StyledInputs
                  value={this.state.password}
                  autocomplete="off"
                  // aria-label="Email"
                  //   style={{
                  //     backgroundImage: `url(${EmailIcon})`,
                  //     backgroundPositionX: '0.4rem',
                  //   }}
                  onChange={this.handlepassword}
                    error={this.state.error}
                  type="password"
                  placeholder={"Password"}
                  name="password"
                />
              </div>
              <BlueButtonNew
                disabled={
                  this.state.userName === "" && this.state.password === ""
                }
                onClick={this.handleLogin}
              >
                LOGIN
              </BlueButtonNew>
             
            </PartnerWithUsForm>
          </FormComponentRight>
        </FormComponent>
      </BookDemoContainer>
    );
  }
}
export default Login;
