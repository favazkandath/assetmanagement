import React from 'react';
import './App.css';
import { Route, withRouter } from "react-router-dom"
import Login from './main/login/Login'
import Dashboard from './main/dashboard/Dashboard'

class Routes extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            loginResponse: localStorage.getItem('isLoggedIn') === "true" ? true : false
        }
    }


    handleLogin = (loginResponse: any) => {
        localStorage.setItem('isLoggedIn', "true")
        this.setState({
            loginResponse
        }, () => this.props.history.push('/homepage'))
    }
    
    handleLogout = () => {
        localStorage.setItem('isLoggedIn', 'false')
    }

    render() {
        if(this.state.loginResponse === false && this.props.history.location.pathname !== '/' && this.props.history.location.pathname !== '/login') {
            this.props.history.push('/login')
        } else if (this.state.loginResponse === true && (this.props.history.location.pathname === '/' || this.props.history.location.pathname === '/login'|| this.props.history.location.pathname === '/Login')) {
            this.props.history.push('/homepage')
        }
        return (<>
            {!this.state.loginResponse ? (<>
                <Route exact={true} path="/login" render={(props: any) => <Login {...props} handleLogin={this.handleLogin} />} />
                <Route exact={true} path="/" render={(props: any) => <Login {...props} handleLogin={this.handleLogin} />} />
            </>) : (<>
                <Dashboard/>
            </>)}

        </>)
    }
}

export default withRouter(Routes)